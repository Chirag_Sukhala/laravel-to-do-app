@extends('layouts.app')

@section('content')
<div class="container">
                @if (Auth::check())
                        <div id="myDIV" class="header">
                            <h2>My To Do List</h2>
                            <input type="text" id="myInput" placeholder="Title...">
                            <span onclick="newElement()" class="addBtn">Add</span>
                        </div>

                        <ul id="myUL">@foreach($user->tasks as $task)
                            

                            <li class="task-list @if($task->checked == 1) checked @endif" id="task:{{$task->id}}">{{$task->description}} <span onclick="deleteTask({{$task->id}})" class="close">&#215</span></li>
                            @endforeach
                        </ul> 

                        
                @else
                    <h3>You need to log in. <a href="/login">Click here to login</a></h3>
                @endif
               
</div>
@endsection
