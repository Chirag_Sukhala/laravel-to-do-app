<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Task;

class TasksController extends Controller
{
    public function index()
    {
    	$user = Auth::user();
    	return view('welcome',compact('user'));
    }

    
    
    //api calls
    public function updateapi(Request $request, Task $task)
    {
    	
            if(isset($request->delete) AND $request->delete == TRUE) {
                $task->delete();
                return response("Deleted Task.", 200);
            }
            elseif (isset($request->edit) AND $request->edit == TRUE)
            {
                $task->description = $request->description;
                $task->save();
                return response("edited Task.", 200);
            }   
            elseif (isset($request->checked))
            {
                $task->checked = $request->checked;
                $task->save();
                return response("checked Task.", 200);
            }  	
    		
    }
    public function createapi(Request $request)
    {
    	$task = new Task();
    	$task->description = $request->description;
    	$task->user_id = Auth::id();
    	$task->save();
        return response()->json([
            'error' => false,
            'taskid'  => $task->id,
        ], 200);
    }

}
