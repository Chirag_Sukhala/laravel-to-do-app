
function getMeta(metaName) {
  const metas = document.getElementsByTagName('meta');

  for (let i = 0; i < metas.length; i++) {
    if (metas[i].getAttribute('name') === metaName) {
      return metas[i].getAttribute('content');
    }
  }

  return '';
}

function deleteTask(id){
  var prompt =  confirm(" Are you sure you want to delete this task?  This action cannot be undone."); 
  if (prompt == true) {
    //do some ajax calls
      var xhttp = new XMLHttpRequest();
      xhttp.open("POST", "/taskapi/"+id, true);
      xhttp.setRequestHeader("X-CSRF-TOKEN", getMeta("csrf-token"));
      xhttp.setRequestHeader("Content-Type", "application/json");
      xhttp.send(JSON.stringify({delete:true}));
    //after confirmation remove from front end
      var div = document.getElementById("task:"+id);
      div.style.display = "none";
  }
}

// Add a "checked" symbol when clicking on a list item
//add deleay for confirmation -pending
var list = document.querySelector('#myUL');
list.addEventListener('click', function(ev) {
  if (ev.target.tagName === 'LI') {
    ev.target.classList.toggle('checked');
    //retrive list id
    var listid = ev.target.id;
    listid = listid.replace("task:","");
    //calculate checked value
    if(ev.target.classList.contains('checked'))
    {
      check = 1;
    } else {
      check = 0;
    }
    //call ajax
    var xhttp = new XMLHttpRequest();
      xhttp.open("POST", "/taskapi/"+listid, true);
      xhttp.setRequestHeader("X-CSRF-TOKEN", getMeta("csrf-token"));
      xhttp.setRequestHeader("Content-Type", "application/json");
      xhttp.send(JSON.stringify({checked: check}));
  }
}, false);

// Create a new list item when clicking on the "Add" button
function newElement() {
  var li = document.createElement("li");
  var inputValue = document.getElementById("myInput").value;
  var t = document.createTextNode(inputValue);
  li.appendChild(t);
  if (inputValue === '') {
    alert("You must write something!");
  } else {
    //do ajax, req id, append id
      var taskid = 0;
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            taskid = JSON.parse(this.responseText).taskid;
            li.setAttribute("id", "task:"+taskid);
            li.setAttribute("class", "task-list");

            span.setAttribute("onclick","deleteTask("+taskid+")");
       }
    };
      xhttp.open("POST", "/taskapi", true);
      xhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      xhttp.setRequestHeader("X-CSRF-TOKEN", getMeta("csrf-token"));
      xhttp.setRequestHeader("Content-Type", "application/json");
      xhttp.send(JSON.stringify({description:inputValue}));

    document.getElementById("myUL").appendChild(li);
  }
  document.getElementById("myInput").value = "";

  var span = document.createElement("SPAN");
  var txt = document.createTextNode("\u00D7");
  span.className = "close";
  span.appendChild(txt);
  li.appendChild(span);


  
} 